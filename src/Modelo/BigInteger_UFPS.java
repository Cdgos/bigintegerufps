/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class BigInteger_UFPS {

    
    /**
     *  Numero="23456"
     * miNUmero={2,3,4,5,6};
     */
    private int miNumero[];
    
    private int num;
    
    public BigInteger_UFPS() {
    }
    
    
    public BigInteger_UFPS(String miNumero) throws Exception{
        
         this.miNumero = new int[miNumero.length()];
            for (int i = 0; i < this.miNumero.length; i++) {
                if(miNumero.charAt(i)<48 || miNumero.charAt(i)>57){
                  throw new Exception("Por favor ingrese solo números");
                }
                this.miNumero[i] = miNumero.charAt(i)-48;
                
            }
            
            num = Integer.parseInt(miNumero);

        
    }


    public int[] getMiNumero() {
        return miNumero;
    }
    
    
    /**
     * Mutiplica dos enteros BigInteger
     * @param dos
     * @return 
     */
    
public BigInteger_UFPS multiply(BigInteger_UFPS dos)
    {
     BigInteger_UFPS result= new BigInteger_UFPS();
     result.miNumero= reverseArr(multiplyVector(reverseArr(this.miNumero), reverseArr(dos.miNumero)));

     return result;
    
    }
   private int[] reverseArr(int []x){
   int []result= new int [x.length];
  
   for(int i=x.length-1; i>=0; i--){
       result[x.length-1-i]=x[i];
   
   }
   
    return result;
   }
    private int [] multiplyVector (int[]x, int[]y){
     
    int [] result= new int [x.length+y.length+1];
 
    for(int i=0; i<x.length;i++){
        for(int j=0; j<y.length;j++){
            int mt= result[i+j]+x[i]*y[j];
            result[i+j]=mt%10;
            result[i+j+1]+=mt/10;
        }
    }
    
    return result;
    }
         
    
    /**
     * Retorna la representación entera del BigInteger_UFPS
     * @return un entero
     */
    public int intValue()
    {
        return num;
    }

    @Override
    public String toString() {
        String x = "";
        
        boolean y = false;
        
        for (int i = 0; i < miNumero.length; i++) {
            if(miNumero[i]!=0 || y==true){
             x+=miNumero[i];
             y = true;  
            }

        }
        
        return x;
    }
    
    
    
    
    
    
    
    
    
    
}
